<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');


Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'API\UserController@details');
	Route::get('logout','API\UserController@logout');

	Route::get('show','BLogController@index');
	Route::post('create','BLogController@create');
    
});

// Route::get('show', 'BookController@index');


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

    //Route::get('show','BLogController@index');
    //Route::post('create','BLogController@create');
    Route::put('update/{id}','BLogController@update');
    Route::delete('delete/{id}','BLogController@delete');