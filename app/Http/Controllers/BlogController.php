<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\User;

use Illuminate\Support\Facades\Auth;
use Lcobucci\JWT\Parser;

class BlogController extends Controller
{
	public $successStatus = 200;

    public function index(){
    	$email = Auth::user()->email;
    	$blogs = Blog::all()->where('email_owner',$email);
        return Response()->json($blogs);

    }
    
    //save data
    public function create(request $request){
    	$email = Auth::user()->email;
        $table = new Blog;
        $table->title = $request->title;
        $table->post_detail = $request->post_detail;
        $table->email_owner=$email;
        $table->save();
        return response()->json('success');

    }
    
    //update data
    public function update(request $request, $id){
        $t = $request->title;
        $p = $request->post_detail;
        
        $table = Blog::find($id);
        $table->title = $t;
        $table->post_detail = $p;
        $table->save();
        
        return "Data updated";
    }
    
    //delete
    public function delete($id){
    $table = Blog::find($id);
    $table->delete();
    
    return "Data deleted";
    }
}
