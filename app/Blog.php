<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
   protected $fillable = array('title','post_detail','email_owner');
}
